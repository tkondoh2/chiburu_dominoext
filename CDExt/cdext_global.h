#ifndef CDEXT_GLOBAL_H
#define CDEXT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CDEXT_LIBRARY)
#  define CDEXTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CDEXTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CDEXT_GLOBAL_H
